<?php

/**
 * Добавляем таб Комментарии в карточке товара
 *
 * Хук fn_set_hook('get_product_tabs_post', $tabs, $lang_code);
 */

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_register_hooks('get_product_tabs_post');

<?php

/**
 * Добавляем таб Комментарии в карточке товара
 *
 * Хук fn_set_hook('get_product_tabs_post', $tabs, $lang_code);
 */

function fn_ximbo_test_get_product_tabs_post(&$tabs, $lang_code) {
    $tabs[] = [
        //'tab_id' => 18,
        'tab_type' => 'T',
        //'block_id' => 0,
        'template' => 'addons/ximbo_test/blocks/product_tabs/comments.tpl',
        'addon' => 'ximbo_test',
        //'position' => 18,
        'status' => 'A',
        'is_primary' => 'Y',
        //'product_ids' => '',
        //'company_id' => 1,
        'show_in_popup' => 'N',
        'lang_code' => $lang_code,
        'name' => fn_get_lang_var('ximbo_test.comments'),
        //'type' => '',
        //'properties' => [],
        //'items_ids' => [],
        //'items_count' => 1,
        'html_id' => 'commets',
    ];
}
<div class="control-group">
    <label for="product_ximbo_author" class="control-label">{__("ximbo_test.author")}</label>
    <div class="controls">
        <input class="input-large" form="form" type="text" name="product_data[ximbo_author]" id="product_ximbo_author" size="55" value="{$product_data.ximbo_author}" />
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="elm_product_ximbo_type">{__("ximbo_test.type")}:</label>
    <div class="controls">
        <select class="span3" name="product_data[ximbo_type]" id="elm_product_ximbo_type" {if $disable_selectors}disabled="disabled"{/if}>
            <option value="roman" {if $product_data.ximbo_type == "roman"}selected="selected"{/if}>{__("ximbo_test.type.roman")}</option>
            <option value="novel" {if $product_data.ximbo_type == "novel"}selected="selected"{/if}>{__("ximbo_test.type.novel")}</option>
            <option value="story" {if $product_data.ximbo_type == "story"}selected="selected"{/if}>{__("ximbo_test.type.story")}</option>
            <option value="poem" {if $product_data.ximbo_type == "poem"}selected="selected"{/if}>{__("ximbo_test.type.poem")}</option>
        </select>
    </div>
</div>

<div class="control-group cm-no-hide-input">
    <label class="control-label" for="elm_product_ximbo_comments">{__("ximbo_test.comments")}:</label>
    <div class="controls">
        <textarea id="elm_product_ximbo_comments" name="product_data[ximbo_comments]" cols="55" rows="8" class="cm-wysiwyg input-large">{$product_data.ximbo_comments}</textarea>
    </div>
</div>
